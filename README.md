# SimBaIT Red Discord bot's cogs
[![Discord server](https://discordapp.com/api/guilds/321640020291092483/embed.png)](https://discord.gg/AebwmFN)
[![Red-DiscordBot](https://img.shields.io/badge/Red--DiscordBot-V3-red.svg)](https://github.com/Cog-Creators/Red-DiscordBot)
[![Discord.py](https://img.shields.io/badge/Discord.py-rewrite-blue.svg)](https://github.com/Rapptz/discord.py/tree/rewrite)
[![License: GPL v3](https://img.shields.io/badge/License-GPLv3-blue.svg)](https://www.gnu.org/licenses/gpl-3.0)

These cogs are for [Red-DiscordBot](https://github.com/Cog-Creators/Red-DiscordBot).  
For any support you can open an [issue on git](https://gitlab.com/SimBaIT/simbait-cogs/-/issues/new) or send me a message on my [discord server](https://discord.gg/aebwmfn/).

## Description of my cogs

| Name | Status/Version | Description (Click to see full status) |
| --- | --- | --- |
| reactrename | **release** : 0.9 | <details><summary>A cog to rename an user when he click on a reaction!</summary>This is my first cogs may are some bug and doesnt may manage the case in the user change itself is nick name after click on the reaction.</details> |
|suggestion|WIP|<details><summary>A cog to creat and manage suggestion</summary>I was strongly inspired by [Poller](https://top.gg/bot/689279941870747904)</details>|

<!-- 
### Rewritten Cogs Originally Made by others

*These cogs were originally written by others.*

| Name | Status/Version | Description (Click to see full status) | Author oririginal |
| --- | --- | --- | --- |
-->

## How to install

*`[p] is your prefix`*

- Make sure Downloader is loaded:<br>
  `[p]load downloader`

- Add the repo to your bot:<br>
  `[p]repo add SimBaIT-cogs https://gitlab.com/SimBaIT/simbait-cogs.git`

- Install the cogs you want:<br>
  `[p]cog install SimBaIT-cogs <cogName>`

- Load installed cogs:<br>
  `[p]load <cogName>`

<!-- ## Credits -->