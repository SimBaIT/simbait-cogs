import discord
from redbot.core import commands, checks, utils
from redbot.core.config import Config

class ReactionRenameUser(commands.Cog):
    """My custom cog"""
    
    def __init__(self, bot):
        self.config = Config.get_conf(
            self, 
            identifier=186211684409409548, 
            force_registration=True
        )
        
        self.bot = bot
        self.config.register_channel(messageID={})

    @commands.guild_only()
    @commands.command()
    async def reactrename(
        self, 
        ctx: commands.GuildContext, 
        channel: discord.TextChannel,
        msgid: int,
        emoji: str,
        position: str,
        text: str = "",
        ):
        """
        Rename an user when he click on a reaction
        
        **position** : the position of the texte, must be end or beg for beginning
        **text** : is optional, in this case emoji will be use. Use `_` for blank
        """
        try:
            message = await channel.fetch_message(msgid)
        except discord.HTTPException:
            return await ctx.maybe_send_embed("No such message")
        
        if text == "":
          text = emoji

        if (position != 'beg') and (position != 'end'):
          return await ctx.maybe_send_embed("Syntaxe error :\n**position** must be `end` or `beg`")

        cfg = self.config.channel(channel).messageID

        if str(msgid) in await cfg.get_raw() and len(await cfg.get_raw(msgid, "reactions")):
            reactions = await cfg.get_raw(msgid, "reactions")
            reactions[emoji] = {"position": position, "text": text}
            await cfg.set_raw(message.id, value={"reactions": reactions})
        else: 
          await cfg.set_raw(message.id, value={"reactions": {emoji: {"position": position, "text":text}}})

        if not any(str(r) == emoji for r in message.reactions):
            try:
                await message.add_reaction(emoji)
                await ctx.tick()
            except discord.HTTPException:
                return await ctx.maybe_send_embed("Hmm, that message couldn't be reacted to")

    @commands.Cog.listener()
    async def on_raw_reaction_add(
        self,
        payload: discord.raw_models.RawReactionActionEvent
        ):

        member = payload.member
        emoji = payload.emoji
        msgid = payload.message_id
        channel = self.bot.get_channel(int(payload.channel_id))

        allConfig = await self.config.all_channels()

        if not msgid in allConfig:
          return

        cfg = self.config.channel(channel).messageID
        position = await cfg.get_raw(msgid, "reactions", emoji, "position")
        text = await cfg.get_raw(msgid, "reactions", emoji, "text")
        
        if member.nick is None: 
          memberName = member.name
        else:
          memberName = member.nick
        
        if not member.bot and not text in memberName and not text == "_":
            if position == "beg":
              newMemberName = text + " " + memberName
            if position == "end":
              newMemberName = memberName + " " + text
            await member.edit(nick=newMemberName)

    @commands.Cog.listener()
    async def on_raw_reaction_remove(
        self, 
        payload: discord.raw_models.RawReactionActionEvent
        ):

        emoji = payload.emoji
        msgid = payload.message_id
        channel = self.bot.get_channel(int(payload.channel_id))
        guild = self.bot.get_guild(int(payload.guild_id))
        member = guild.get_member(int(payload.user_id))

        allConfig = await self.config.all_channels()

        if not msgid in allConfig:
          return
        
        cfg = self.config.channel(channel).messageID
        text = await cfg.get_raw(msgid, "reactions", emoji, "text")

        if member.nick is None: 
          memberName = member.name
        else:
          memberName = member.nick

        if not member.bot and text in memberName:
          begPos = memberName.find(text)
          endPos = begPos + len(text)
          if begPos == 0:
            newMemberName = memberName[endPos:]
          elif endPos == len(memberName):
            newMemberName = memberName[:endPos+1]
          else:
            newMemberName = memberName[:begPos-1]
            newMemberName += memberName[endPos+1:]
          await member.edit(nick=newMemberName)

    # @commands.Cog.listener()
    # async def on_member_update(
    #     self,
    #     before : discord.member,
    #     after : discord.member
    #     ):

    #     allConfig = await self.config.all_channels()
    #     # print(before.nick)
    #     # print(after.nick)

