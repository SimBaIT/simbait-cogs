import discord
from datetime import datetime
from redbot.core import commands, checks, utils
from redbot.core.config import Config

default_vup_emoji = "\N{Thumbs Up Sign}"
default_vdown_emoji = "\N{Thumbs Down Sign}"
default_accept_emoji = "\N{WHITE HEAVY CHECK MARK}"
default_deny_emoji = "\N{No Entry}"
default_del_emoji = "\N{Cross Mark}"  

default_settings = {
    "enable" : None,
    "sub_channel" : None,
    "accept_channel" : None,
    "deny_channel" : None,
    "del_channel" : None,
    "admin_roles" : [None],
    "vote_roles" : [None],
    "suggest_roles" : [None],
    "vup_emoji" : default_vup_emoji,
    "vdown_emoji" : default_vdown_emoji,
    "accept_emoji" : default_accept_emoji,
    "deny_emoji" : default_deny_emoji,
    "del_emoji" : default_del_emoji,
    "embed_data" : {
        "colour": None,
        "footer": None,
        "thumbnail": None,
        "image": None,
        "icon_url": None,
        "author": None,
        "timestamp": True,
    },
    "messages" : {}
}

class suggestion(commands.Cog):
    """
    Suggestion box with voting system.
    **Use `[p]setsuggest setup` first.**
    Only admins can approve or reject suggestions.
    """

    def __init__(self, bot):
        self.config = Config.get_conf(
            self, 
            identifier=186211684409409548, 
            force_registration=True
        )

        self.bot = bot
        self.config.register_guild(**default_settings)

    @commands.group(autohelp=False)
    @commands.guild_only()
    @checks.admin_or_permissions(administrator=True)
    async def suggestionset(self, ctx: commands.Context):
        """Set parameters for suggestion"""

        guild = ctx.message.guild
        cfg = self.config.guild(guild)
        setting_names = {
            "enable" : "**Enable**",
            "sub_channel" : "**Submit channel**",
            "accept_channel" : "**Accepted suggestions channel**",
            "deny_channel" : "**Refused suggestions channel**",
            "del_channel" : "**Deleted suggestions channel**",
            "admin_roles" : "**Admin roles**",
            "vote_roles" : "**Vote roles**",
            "suggest_roles" : "**Suggest roles**",
            "vup_emoji" : "**Vote up emoji**",
            "vdown_emoji" : "**Vote down emoji**",
            "accept_emoji" : "**Accept emoji**",
            "deny_emoji" : "**Refuse emoji**",
            "del_emoji" : "**Delete emoji**",
            # "embed_data" : {
            #     "title": "**Titre**",
            #     "colour": "**Color**",
            #     "footer": "**Footer**",
            #     "thumbnail": "**Thumbnail**",
            #     "image": "**Image**",
            #     "icon_url": "**Icone**",
            #     "author": "**Autor**",
            # }
        }

        if ctx.invoked_subcommand is None:
            await ctx.send_help()
            if ctx.channel.permissions_for(ctx.me).embed_links:
                embed = discord.Embed(colour=await ctx.embed_colour())
                embed.set_author(name=("Suggestion settings for ") + guild.name)
                msg = ""
                # field_msg = ""
                for attr, name in setting_names.items():
                    if not attr == "embed_data":
                        if "channel" in attr:
                            chan = guild.get_channel(await cfg.get_raw(attr))
                            if chan is not None:
                                msg += name + " : " + chan.mention + "\n"
                            else:
                                msg += name + " : " + str(await cfg.get_raw(attr)) + "\n"
                        elif "role" in attr:
                            roles = await cfg.get_raw(attr)
                            rolesName = ""
                            for role in roles:
                                if role is not None:
                                    role = guild.get_role(role)
                                    if rolesName : 
    	                                rolesName += ", "
                                    if "everyone" in role.name:
                                        rolesName += role.name
                                    else:
                                        rolesName += role.mention
                                else:
                                     rolesName = str(role)
                            msg += name + " : " + rolesName + "\n"
                        else:
                            msg += name + " : " + str(await cfg.get_raw(attr)) + "\n"
                    # else : 
                    #     for attr, name in setting_names["embed_data"].items():
                    #         field_msg += name + " : " + str(await cfg.embed_data.get_raw(attr)) + "\n"
            embed.description = msg
            # embed.add_field(
            #     name="__**Suggest embed :**__",
            #     value=field_msg,
            #     inline=False,
            # )
            await ctx.send(embed=embed)

    @suggestionset.command(name="toggle")
    async def suggestionset_toggle(self, ctx: commands.Context) -> None:
        """
            Toggle state (enable/disable)
        """
        
        guild = ctx.message.guild
        cfg = self.config.guild(guild)
        default_role = guild.default_role
        msg = ""

        if await cfg.get_raw("enable") is None:
            msg = "Suggestion are now enable but not config"
            await cfg.set(default_settings)
            await cfg.enable.set(True)
            await cfg.vote_roles.set([default_role.id])
            await cfg.suggest_roles.set([default_role.id])
        elif await cfg.get_raw("enable") is False:
            msg = "Suggestion are now enable"
            await cfg.enable.set(True)
        elif await cfg.get_raw("enable") is True:
            msg = "Suggestion are now disable"
            await cfg.enable.set(False)

        await ctx.send(msg)

    @suggestionset.command(name="roles")
    async def suggestionset_roles(self, ctx: commands.Context, action : str, role : discord.Role = None) -> None:
        """
            Manage role can accept or refuse a suggestion

            **admin** : for administration roles
            **vote** : for vote roles
            **suggest** : for suggest roles
        """
        
        guild = ctx.message.guild
        cfg = self.config.guild(guild)
        default_role = guild.default_role
        
        rolesCfg =  await cfg.get_raw(action + "_roles")
        roles = ""
    
        if role and not role.id in rolesCfg:
            if not None in rolesCfg and not default_role.id in rolesCfg:
                roles = rolesCfg
                roles.append(role.id)
            else:
                roles = [role.id]
            if action == "admin":
                await cfg.admin_roles.set(roles)
            if action == "vote":
                await cfg.vote_roles.set(roles)
            if action == "suggest":
                await cfg.suggest_roles.set(roles)
        else:
            if action == "admin":
                await cfg.admin_roles.set([None])
            if action == "vote":
                await cfg.vote_roles.set([default_role.id])
            if action == "suggest":
                await cfg.suggest_roles.set([default_role.id])
        
        await ctx.tick()

    @suggestionset.command(name="channels")
    async def suggestionset_channels(self, ctx: commands.Context, action : str, channel : discord.TextChannel) -> None:
        """
            Set suggestion channel

            **sub** : for submit
            **accept** : for accept
            **deny** : for reject
            **del** : for delete
        """
        
        guild = ctx.message.guild
        cfg = self.config.guild(guild)

        if action == "sub":
            await cfg.sub_channel.set(channel.id)
        if action == "accept":
            await cfg.accept_channel.set(channel.id)
        if action == "deny":
            await cfg.deny_channel.set(channel.id)
        if action == "del":
            await cfg.del_channel.set(channel.id)

        await ctx.tick()

    @suggestionset.command(name="emojis")
    async def suggestionset_emojis(self, ctx: commands.Context, action : str, emoji : str) -> None:
        """
        Set the emoji for each action

        **vup** : for vote up
        **vdown** : for vote up
        **accept** : for accept
        **deny** : for reject
        **del** : for delete
        """

        guild = ctx.message.guild
        cfg = self.config.guild(guild)

        if action == "vup":
            await cfg.vup_emoji.set(emoji)
        if action == "vdown":
            await cfg.vdown_emoji.set(emoji)
        if action == "accept":
            await cfg.accept_emoji.set(emoji)
        if action == "deny":
            await cfg.deny_emoji.set(emoji)
        if action == "del":
            await cfg.del_emoji.set(emoji)

        await ctx.tick()
    
    @commands.guild_only()
    @commands.command()    
    async def suggest(self, ctx: commands.Context, title : str, desc : str):
        """
            Submit a new suggestion.
            Be careful with the syntaxe : "title" "description".
            If you forget the quotation mark `"` and you have spaces in you title or description,
            the first word will be use for title and the second for description
        """
        
        guild = ctx.message.guild
        channel = ctx.channel
        cfg = self.config.guild(guild)
        
        rightRole = False
        rightChannel = False

        if ctx.author.guild_permissions.administrator or ctx.author.guild_permissions.manage_channels:
            rightRole = True
        else:
            for role_id in await cfg.get_raw("suggest_roles"):
                role = guild.get_role(role_id)
                if role in ctx.author.roles:
                    rightRole = True

        if channel.id == await cfg.get_raw("sub_channel"):
            rightChannel = True
        
        print(channel.id)
        print(await cfg.get_raw("sub_channel"))

        if rightRole and rightChannel:
            ctx.author.avatar_url
            embed = discord.Embed(colour=await ctx.embed_colour())
            embed.set_author(name=ctx.author, icon_url=ctx.author.avatar_url)
            embed.set_thumbnail(url=ctx.guild.icon_url)
            embed.title = title
            embed.description = desc
            
            sendMsg= await ctx.send(embed=embed)
            await sendMsg.add_reaction(await cfg.get_raw("vup_emoji"))
            await sendMsg.add_reaction(await cfg.get_raw("vdown_emoji"))
            await sendMsg.add_reaction(await cfg.get_raw("accept_emoji"))
            await sendMsg.add_reaction(await cfg.get_raw("deny_emoji"))
            await sendMsg.add_reaction(await cfg.get_raw("del_emoji"))
            msgCfg = await cfg.get_raw("messages")
            msgCfg[sendMsg.id] = {"author": ctx.author.id, "timestamp": str(datetime.utcnow()), "vup": 0, "vdown": 0}
            await cfg.set_raw("messages", value= msgCfg)
        else:
            if not rightRole: 
                roles = await cfg.get_raw("suggest_roles")
                rolesName = ""
                for role in roles:
                    role = guild.get_role(role)
                    if rolesName : 
                        rolesName += ", "
                    rolesName += role.mention
                await ctx.send("You haven't right to suggest something.\nYou must be member of " + rolesName, delete_after=30)
            if not rightChannel:
                suggestChannel = guild.get_channel(await cfg.get_raw("sub_channel"))
                await ctx.send("You can doing a suggestion only in " + suggestChannel.mention, delete_after=30)
        await ctx.message.delete()
    
    @commands.Cog.listener()
    async def on_raw_reaction_add(
        self,
        payload: discord.raw_models.RawReactionActionEvent
        ):

        emoji = payload.emoji
        msgid = payload.message_id
        guild = self.bot.get_guild(int(payload.guild_id))
        member = guild.get_member(int(payload.user_id))
        channel = self.bot.get_channel(int(payload.channel_id))
        
        cfg = self.config.guild(guild)
        messages = await cfg.get_raw("messages")
        
        try:
            message = await channel.fetch_message(msgid)
        except discord.HTTPException:
            return await ctx.maybe_send_embed("No such message") 

        if not str(msgid) in messages:
            return

        suggest_admin = False
        can_vote =  False
        action = None

        if member.guild_permissions.administrator or member.guild_permissions.manage_channels:
            suggest_admin = True
            can_vote = True
        else:
            for role_id in await cfg.get_raw("admin_roles"):
                role = guild.get_role(role_id)
                if role in member.roles:
                    suggest_admin = True
                    can_vote = True

        for role_id in await cfg.get_raw("vote_roles"):
            role = guild.get_role(role_id)
            if role in member.roles:
                can_vote = True

        vup_emoji = await cfg.get_raw("vup_emoji")
        vdown_emoji = await cfg.get_raw("vdown_emoji")
        accept_emoji = await cfg.get_raw("accept_emoji")
        deny_emoji = await cfg.get_raw("deny_emoji")
        del_emoji = await cfg.get_raw("del_emoji")

        vup = await cfg.get_raw("messages", msgid, "vup")
        vdown = await cfg.get_raw("messages", msgid, "vdown")
        author = await cfg.get_raw("messages", msgid, "author")
        timestamp = await cfg.get_raw("messages", msgid, "timestamp")

        msg = message.embeds[0]

        if str(emoji) == vup_emoji:
            if not member.bot and can_vote:
                vup += 1
                await cfg.set_raw("messages", msgid, "vup", value=vup)
                await message.remove_reaction(vdown_emoji, member)
        if str(emoji) == vdown_emoji:
            if not member.bot and can_vote:
                vdown += 1
                await cfg.set_raw("messages", msgid, "vdown", value=vdown)
                await message.remove_reaction(vup_emoji, member)
        if str(emoji) == accept_emoji:
            if not member.bot and suggest_admin:
                chan = guild.get_channel(await cfg.get_raw("accept_channel"))
                await chan.send(embed=msg)
                await message.delete()
            elif not member.bot:
                await message.remove_reaction(accept_emoji, member)
        if str(emoji) == deny_emoji:
            if not member.bot and suggest_admin:
                chan = guild.get_channel(await cfg.get_raw("deny_channel"))
                await chan.send(embed=msg)
                await message.delete()
            elif not member.bot:
                await message.remove_reaction(deny_emoji, member)
        if str(emoji) == del_emoji:
            if not member.bot and (suggest_admin or member.id == author):
                chan = guild.get_channel(await cfg.get_raw("del_channel"))
                await chan.send(embed=msg)
                await message.delete()
            elif not member.bot:
                await message.remove_reaction(del_emoji, member)                   

    @commands.Cog.listener()
    async def on_raw_reaction_remove(
        self, 
        payload: discord.raw_models.RawReactionActionEvent
        ):

        emoji = payload.emoji
        msgid = payload.message_id
        guild = self.bot.get_guild(int(payload.guild_id))
        member = guild.get_member(int(payload.user_id))
        
        cfg = self.config.guild(guild)
        messages = await cfg.get_raw("messages")

        if not str(msgid) in messages:
            return

        vup_emoji = await cfg.get_raw("vup_emoji")
        vdown_emoji = await cfg.get_raw("vdown_emoji")

        vup = await cfg.get_raw("messages", msgid, "vup")
        vdown = await cfg.get_raw("messages", msgid, "vdown")

        if str(emoji) == vup_emoji:
            if not member.bot:
                vup -= 1
                await cfg.set_raw("messages", msgid, "vup", value=vup)
        if str(emoji) == vdown_emoji:
            if not member.bot:
                vdown -= 1
                await cfg.set_raw("messages", msgid, "vdown", value=vdown)